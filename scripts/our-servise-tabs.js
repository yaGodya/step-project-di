const tabsOurServise = document.querySelectorAll(".our-servise-list-item");
const tabsOurServiseItems = document.querySelectorAll(".our-servise-list-description-item");

tabsOurServise.forEach(function (item) {
    item.addEventListener("click", () => {
        let currentBtn = item;
        let tabsId = currentBtn.getAttribute("data-tab");
        let currentTab = document.querySelector(tabsId);
        
        tabsOurServiseItems.forEach(function (item) {
            item.classList.remove('active') });

        tabsOurServise.forEach(function (item) {
            item.classList.remove('active') });

        currentBtn.classList.add('active');
        currentTab.classList.add('active');
    })
});

