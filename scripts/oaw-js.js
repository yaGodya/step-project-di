const tabsOaw = document.querySelectorAll(".oaw-tabs-item");
const tabsOawItems = document.querySelectorAll(".oaw-cards-img");

tabsOaw.forEach(function (item) {

    item.addEventListener("click", () => {
        let currentBtn = item;
        let tabDataAtr = currentBtn.getAttribute("data-tab-oaw");
        let tabImgs = document.querySelectorAll(`[data-tab-oaw-img=${tabDataAtr}]`);
        
        tabsOaw.forEach(function(elem) { 
            elem.classList.remove('active'); 
        })
        currentBtn.classList.add('active');

        if (tabDataAtr === 'oaw-tab_all') { 
            tabsOawItems.forEach((elem) => {
                elem.classList.add('active')
            });
            return;
        };

        tabsOawItems.forEach(function (item) {
            item.classList.remove('active') 
        });
        tabImgs.forEach(function (item) {
            item.classList.add('active') 
        });   
    })
});

const loadCards = document.querySelectorAll('.load');
const loadButton = document.querySelector('.load-more');
const beforeLoad = document.querySelector('.load-wrap');
const loader = document.querySelector('.loader');

loadButton.addEventListener('click', () => {
    beforeLoad.style.display = 'none';
    loader.style.display = 'block';

    setTimeout(() => {
        loadCards.forEach((elem) => {
            elem.classList.remove('load');
            loadButton.style.display = 'none';
        })
    }, 800);
})



