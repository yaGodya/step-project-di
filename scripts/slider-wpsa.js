const sliderLine = document.querySelector('.wpsa-slider-line');
const prevBtn = document.querySelector('.wpsa-prev-button');
const nextBtn = document.querySelector('.wpsa-next-button');
const dots = document.querySelectorAll('.wpsa-dot');

let position = 0;
let dotIndex = 0; 

const nextSlide = () => {
    if (position < ((dots.length - 1) * 1160)) { 
        position += 1160;
        dotIndex++;
    } else {
        position = 0;
        dotIndex = 0;
    }
    sliderLine.style.left = -position + 'px';
    thisSlide(dotIndex);
}

const prevSlide = () => {
    if (position > 0) { 
        position -= 1160;
        dotIndex--
    } else {
        position = (dots.length - 1) * 1160;
        dotIndex = (dots.length - 1);
    }
    sliderLine.style.left = -position + 'px';
    thisSlide(dotIndex);
}

const thisSlide = (index) => {
    for (let dot of dots) { 
        dot.classList.remove('active');
    }; 
    dots[index].classList.add('active');
}

nextBtn.addEventListener('click', nextSlide);
prevBtn.addEventListener('click', prevSlide);

dots.forEach((dot, index) => {
    dot.addEventListener('click', () => {
        position = 1160 * index;
        sliderLine.style.left = -position + 'px';
        dotIndex = index;
        thisSlide(dotIndex);
    });
});